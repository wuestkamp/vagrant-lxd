#
# Copyright (c) 2018-2019 Catalyst.net Ltd
#
# This file is part of vagrant-lxd.
#
# vagrant-lxd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-lxd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-lxd. If not, see <http://www.gnu.org/licenses/>.
#

require 'lib/vagrant-lxd'
require 'lib/vagrant-lxd/action'
require 'lib/vagrant-lxd/provider'

describe VagrantLXD::Provider do
  let(:config) { VagrantLXD::Config.new.tap(&:finalize!) }
  let(:driver) { double('driver') }
  let(:machine) { double('machine') }
  let(:machine_info) { Hash(host: '127.0.0.1', port: 22) }
  let(:machine_state) { Vagrant::MachineState::NOT_CREATED_ID }

  subject do
    described_class.new(machine).tap do |provider|
      provider.instance_variable_set(:@driver, driver)
    end
  end

  before do
    machine.stub(:provider_config).and_return(config)
    driver.stub(:info).and_return(machine_info)
    driver.stub(:state).and_return(machine_state)
  end

  its('ssh_info') { should == machine_info }
  its('state') { should be_a Vagrant::MachineState }
  its('state.id') { should be Vagrant::MachineState::NOT_CREATED_ID }
  its('state.short_description') { should == 'not created' }
end
