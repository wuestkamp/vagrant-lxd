#
# Copyright (c) 2017-2019 Catalyst.net Ltd
#
# This file is part of vagrant-lxd.
#
# vagrant-lxd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-lxd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-lxd. If not, see <http://www.gnu.org/licenses/>.
#

require 'vagrant-lxd/config'

module VagrantLXD
  class Capability
    def Capability.snapshot_list(machine)
      env = machine.action(:snapshot_list)
      env[:machine_snapshot_list] || []
    end

    def Capability.synced_folders(env)
      logger = Log4r::Logger.new('vagrant::lxd::capability')
      logger.debug "Checking synced folders support for effective UID/GID #{Process.uid}/#{Process.gid}..."
      %w(uid gid).all? do |type|
        begin
          id = Process.send(type)
          id_map = File.readlines("/etc/sub#{type}")
          id_in_sub_id?(id, id_map)
        rescue StandardError => e
          logger.warn "Cannot read subordinate permissions file: #{e.message}"
          false
        end
      end
    end

    # Determines whether the given numerical `id` is included in the
    # subordinate map `id_map`, which should be an array of lines from
    # the file /etc/subuid or /etc/subgid.
    #
    # Invalid lines, and any lines for a user or group other than root,
    # are ignored. See subuid(5) and subgid(5) for details about these
    # files, and the expected format of their entries.
    def Capability.id_in_sub_id?(id, id_map)
      id_map.any? do |line|
        if line.match(/^(root|0):(\d+):(\d+)/)
          range_min = $2.to_i
          range_max = range_min + $3.to_i
          id.between?(range_min, range_max)
        end
      end
    end
  end
end
