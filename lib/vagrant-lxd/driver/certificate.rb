#
# Copyright (c) 2019 Catalyst.net Ltd
#
# This file is part of vagrant-lxd.
#
# vagrant-lxd is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
#
# vagrant-lxd is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with vagrant-lxd. If not, see <http://www.gnu.org/licenses/>.
#

require 'etc'
require 'openssl'
require 'pathname'
require 'socket'
require 'vagrant-lxd/version'

module VagrantLXD
  class Driver
    class Certificate < Struct.new(:certificate, :key)
      PKEY_BITS = 4096
      CERT_EXPIRY_YEARS = 10

      GenerationFailure = Class.new(Exception)

      @logger = Log4r::Logger.new('vagrant::lxd::driver::certificate')

      def Certificate.default_paths
        [
          Pathname.new('~/.config/lxc').expand_path,
          Pathname.new('~/snap/lxd/current/.config/lxc').expand_path,
        ]
      end

      def Certificate.issuer_name
        version = "#{Version::DESCRIPTION} #{Version::VERSION}"
        username = Etc.getpwuid(Process.uid).name
        hostname = Socket.gethostname
        "/O=linuxcontainers.org/OU=#{version}/CN=#{username}@#{hostname}"
      end

      def Certificate.locate(paths)
        if path = paths.find { |x| usable?(x) }
          @logger.debug "Found usable certificate under #{path}"
          Certificate.new(path / 'client.crt', path / 'client.key')
        end
      end

      def Certificate.generate(path)
        @logger.debug 'Generating new client certificate...'
        name = OpenSSL::X509::Name.parse(issuer_name)
        pkey = OpenSSL::PKey::RSA.new(PKEY_BITS)
        cert = OpenSSL::X509::Certificate.new
        cert.serial = 0
        cert.version = 3
        cert.issuer = name
        cert.subject = name
        cert.public_key = pkey.public_key
        cert.not_before = Time.now
        cert.not_after = Time.now + (365 * 24 * 60 * 60 * CERT_EXPIRY_YEARS)
        cert.sign(pkey, OpenSSL::Digest::SHA1.new)
        @logger.debug "Saving new certificate to disk under #{path}..."
        FileUtils.mkdir_p(path, mode: 0o700)
        File.write(path / 'client.crt', cert.to_s, 0, perm: 0o600)
        File.write(path / 'client.key', pkey.to_s, 0, perm: 0o600)
        Certificate.new(path / 'client.crt', path / 'client.key')
      rescue Exception => e
        @logger.error 'Certificate creation failed: ' << e.message
        fail GenerationFailure, e.message
      end

      def Certificate.usable?(path)
        @logger.debug "Checking for existing client certificate in #{path}..."
        OpenSSL::PKey.read(File.read(path / 'client.key'))
        OpenSSL::X509::Certificate.new(File.read(path / 'client.crt'))
      rescue Exception
        false
      else
        true
      end
    end
  end
end
